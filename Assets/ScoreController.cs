﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreController : MonoBehaviour
{
	private Text text;

	public int score = 0;

	private float time = 0;

	private bool counting = false;
	// Use this for initialization
	void Start ()
	{
		text = GetComponent<Text>();
		text.text = "Score: " + score;
		StartCounting();
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (counting)
		{
			time += Time.deltaTime;
			if (time > 1)
			{
				score += Mathf.RoundToInt(time);
				text.text = "Score: " + score;
				time = 0;
			}	
		}		
	}

	public void StartCounting()
	{
		counting = true;
	}
	
	public void StopCounting()
	{
		counting = false;
	}
}
