// Copyright 2014 Google Inc. All rights reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

namespace GoogleVR.HelloVR {
  using UnityEngine;

  public class ObjectController : MonoBehaviour {
    private Vector3 startingPosition;
    private Renderer renderer;

    public Material inactiveMaterial;
    public Material gazedAtMaterial;
    private bool catched = false;
    public bool rotate = false;
    public bool playOnGather;
    

    [SerializeField] private int slowAmount;
    
    Vector3 pickedPosition = new Vector3(0.5f,-0.4f,1);

    void Start() {
      renderer = GetComponent<Renderer>();
      SetGazedAt(false);
    }

    private void Update()
    {
      if (rotate)
      {
        transform.Rotate( new Vector3(10f, 0, 0) );
      }
    }


    public void SetGazedAt(bool gazedAt) {
      if (inactiveMaterial != null && gazedAtMaterial != null && !catched) {
        renderer.material = gazedAt ? gazedAtMaterial : inactiveMaterial;
      }
    }

    public void Catch()
    {
      GameObject player = GameObject.FindGameObjectsWithTag("MainCamera")[0].gameObject;
      PlayerController pc = player.GetComponentInParent<PlayerController>();   
      if (!pc.hasObject && !catched)
      {
        if (playOnGather)
        {
          GetComponent<AudioSource>().Play();
        }
        GetComponent<ItemDestroyer>().destroyable = false;
        catched = true;
        gameObject.GetComponent<Rigidbody>().useGravity = false;
        pc.item = gameObject;
        gameObject.transform.SetParent(player.transform);
        pc.hasObject = true;
        gameObject.transform.localPosition = pickedPosition;
        gameObject.transform.rotation = Quaternion.Euler(player.transform.rotation.eulerAngles + new Vector3(-90f,0,0));
        gameObject.GetComponentInChildren<ParticleSystem>().Clear();
        gameObject.GetComponentInChildren<ParticleSystem>().Stop();
        
        SetGazedAt(false);
      }   
    }

    private void OnCollisionEnter(Collision other)
    {
      if (other.gameObject.tag == "Viking")
      {
        other.gameObject.GetComponent<VikingController>().hit(slowAmount);
        Destroy(gameObject);
      }
      
    }
  }
}
