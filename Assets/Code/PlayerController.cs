﻿using GoogleVR.HelloVR;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{

	public bool hasObject = false;
	public GameObject item;
	private float shootForce = 800f;
	private GameObject camera;
	
	// Use this for initialization
	void Start () {
		camera = GameObject.FindGameObjectWithTag("MainCamera");
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetAxis("Fire1")>0 && hasObject)
		{
			RaycastHit hit;
			Physics.Raycast(transform.position, transform.forward, out hit);
			Vector3 lastPos = item.transform.position;
			Quaternion lastRot = item.transform.rotation;
			item.transform.SetParent(null,true);
			item.transform.position = lastPos;
			item.transform.rotation = lastRot;
			item.layer = LayerMask.NameToLayer("Projectile");
			if (!item.GetComponent<ObjectController>().playOnGather)
			{
				item.GetComponent<AudioSource>().Play();
			}
			
			Vector3 distance = hit.point - item.transform.position;
			ItemDestroyer destroyer = item.GetComponent<ItemDestroyer>();
			if (destroyer)
			{
				destroyer.destroyable = true;
				destroyer.DestroyAfterHit();
			}
			item.GetComponent<Rigidbody>().AddForce(distance/distance.magnitude*shootForce );
			item.GetComponent<ObjectController>().rotate = true;
			hasObject = false;
			item = null;
		}
	}

	public void Die()
	{
		FindObjectOfType<Canvas>().GetComponentInChildren<ScoreController>().StopCounting();
		int highscore = PlayerPrefs.GetInt("highscore");
		int score = FindObjectOfType<Canvas>().GetComponentInChildren<ScoreController>().score;
		if (score > highscore)
		{
			PlayerPrefs.SetInt("highscore",score);
		}
		SceneManager.LoadScene(0);
	}
}
