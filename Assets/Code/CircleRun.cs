﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CircleRun : MonoBehaviour {

	[SerializeField]
	private float MaxSpeed = 1f;
	
	private GameObject mainCamera;
	private GameObject viking;
	private float CurrentSpeed = 0f; // we want to stay at start
	
	void Start ()
	{
		mainCamera = GameObject.FindGameObjectWithTag("MainCamera");
		StartCoroutine(DelayStart());
	}
	
	IEnumerator DelayStart()
	{
		yield return new WaitForSeconds(2);
		CurrentSpeed = MaxSpeed;
	}
	
	void Update ()
	{
		float speed = CurrentSpeed * speedMultiplierBasedOnRotation();
		transform.position += Vector3.back * speed * Time.deltaTime;
	}
	
	float speedMultiplierBasedOnRotation()
	{
		// our viking has 180 at start
		return 1 - Mathf.Abs(Mathf.DeltaAngle(mainCamera.transform.rotation.eulerAngles.y, 180) / 360);
	}

	void OnCollisionEnter(Collision other)
	{
		Debug.Log("dupa");
		if (other.gameObject.tag == "Win")
		{
			int highscore = PlayerPrefs.GetInt("highscore");
			int score = GameObject.FindGameObjectWithTag("Score").GetComponent<ScoreController>().score;
			if (score > highscore)
			{
				PlayerPrefs.SetInt("highscore",score);
			}
			SceneManager.LoadScene(0);
		}
	}
}
