﻿using System.Collections;
using UnityEngine;

public class ItemDestroyer : MonoBehaviour
{
	public float SecondsToDestroy = 3;
	public bool destroyable = true;
	
	void Start()
	{
		StartCoroutine(destroy());
	}

	IEnumerator destroy()
	{
		yield return new WaitForSeconds(SecondsToDestroy);
		if (destroyable)
		{
			Destroy(gameObject);
		}
	}

	public void DestroyAfterHit()
	{
		StartCoroutine(destroy());
	}
}
