﻿using System.Collections;
using UnityEngine;
using UnityEngine.AI;

public class VikingController : MonoBehaviour {

	// Use this for initialization

	private NavMeshAgent agent;
	private Transform player;
	private AudioSource audio;
	private float speedCap = 6;
	private float counter = 0;

	private bool startViking = false;
	
	void Start ()
	{
		audio = GetComponent<AudioSource>();
		agent = gameObject.GetComponent<NavMeshAgent>();
		player = GameObject.FindGameObjectWithTag("Player").transform;
		agent.speed = 0;
		StartCoroutine(DelayVikingStart());
	}

	IEnumerator DelayVikingStart()
	{
		yield return new WaitForSeconds(4);
		startViking = true;
		agent.speed = 1f;
	}
	
	// Update is called once per frame
	void Update () {
		agent.destination = player.position;
		if (agent.speed < speedCap && startViking)
		{
			counter += Time.deltaTime;
			if (counter > 1.5)
			{
				counter = 0;
				agent.speed += 1f;
				if (agent.speed > speedCap)
				{
					agent.speed = speedCap;
				}
			}
		}
		if (agent.remainingDistance < 2)
		{
			TargetReached();
		}
	}

	public void hit(int slowAmount)
	{
		GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraShake>().DoShake();
		//GameObject.FindGameObjectWithTag("Lightning").GetComponent<ParticleSystem>().Play();
		gameObject.transform.position += new Vector3(0,0,5);
		agent.speed -= slowAmount;
		if (agent.speed < 1)
			agent.speed = 1;
		audio.Play();
		
	}
	
	void IncreaseSpeedPerSecond ()
	{
		if (agent.speed < speedCap)
		{		
			agent.speed = agent.speed + 0.5f;
		}	
	}

	void TargetReached()
	{
		GameObject.FindGameObjectWithTag("Player").GetComponentInChildren<PlayerController>().Die();
	}
}
