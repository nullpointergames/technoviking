﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ItemCreator : MonoBehaviour
{
	public int RateOfSpawnSeconds = 2;
	public List<Object> Items = new List<Object>();

	private double nextSpawn = 0;

	private void Start()
	{
		Items = Resources.LoadAll("Items", typeof(Object)).ToList();
	}

	void Update()
	{
		if(Time.time > nextSpawn)
		{
			nextSpawn = Time.time + RateOfSpawnSeconds;
           
			// Random position within Creator box
			Vector3 scale = transform.localScale;
			Vector3 rndPosWithin = new Vector3(Random.Range(-scale.x, scale.x), Random.Range(-scale.y, scale.y), Random.Range(-scale.z, scale.z));
			Vector3 spawnLocation = transform.position + rndPosWithin;

			if (Items.Count > 0)
			{
				GameObject item = (GameObject)Items[Random.Range(0, Items.Count)];
				Instantiate(item, spawnLocation, Quaternion.Euler(new Vector3(-90f,0,0)));
			}
		}
	}
}
