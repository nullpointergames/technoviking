﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class WinScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	private void OnCollisionEnter(Collision other)
	{
		if (other.gameObject.tag == "Player")
		{
			int highscore = PlayerPrefs.GetInt("highscore");
			int score = GameObject.FindGameObjectWithTag("Score").GetComponent<ScoreController>().score;
			if (score > highscore)
			{
				PlayerPrefs.SetInt("highscore",score);
			}
			SceneManager.LoadScene(0);
		}
	}
}
