﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SinusWalk : MonoBehaviour
{

	public float amplitude;
	public float frequency;
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
	{
		float z = transform.position.z;
		//transform.localRotation = Quaternion.Euler(transform.rotation.eulerAngles + new Vector3(0,0,5*Mathf.Sin(transform.position.z-z)));
		transform.position += amplitude*(Mathf.Sin(2*Mathf.PI*frequency*Time.time) - Mathf.Sin(2*Mathf.PI*frequency*(transform.position.z-z)))*transform.up;
	}
}
