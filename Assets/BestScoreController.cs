﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BestScoreController : MonoBehaviour {

	// Use this for initialization
	void Start ()
	{
		int highscore = PlayerPrefs.GetInt("highscore");
		GetComponent<Text>().text = "Best: "+highscore;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
